import 'dart:io' show File, Platform;
import 'package:flutter/material.dart';

import 'package:share_it/share_it.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:path_provider/path_provider.dart' show getExternalStorageDirectory, getApplicationDocumentsDirectory;
import 'package:path/path.dart' show join;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/avatar.png', width: 100),
              TextButton(
                child: Text('Share image'),
                onPressed: () async {
                  ShareIt.file(path: await _imageBundlePath, type: ShareItFileType.image);
                },
              ),
              TextButton(
                child: Text('Share file'),
                onPressed: () async {
                  ShareIt.file(path: await _fileBundlePath, type: ShareItFileType.anyFile);
                },
              ),
              TextButton(
                child: Text('Share link method 1'),
                onPressed: () async {
                  ShareIt.text(content: 'https://www.google.com');
                },
              ),
              TextButton(
                child: Text('Share link method 2'),
                onPressed: () async {
                  ShareIt.link(url: 'https://www.google.com', androidSheetTitle: 'Google');
                },
              ),
              TextButton(
                child: Text('Share image and text'),
                onPressed: () async {
                  ShareIt.list(
                    androidSheetTitle: 'Multiple stuff!',
                    parameters: [
                      ShareItParameters.plainText(content: 'Example text'),
                      ShareItParameters(
                        type: ShareItFileType.image,
                        path: await _imageBundlePath,
                      )
                    ]
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<String> get _imageBundlePath async {
    return _fileFromBundle(name: 'avatar.png');
  }

  Future<String> get _fileBundlePath async {
    return _fileFromBundle(name: 'Main.java');
  }

  //

  Future<String> _fileFromBundle({required String name}) async {
    final directory = Platform.isIOS ? await getApplicationDocumentsDirectory() : await getExternalStorageDirectory();
    if (directory == null) {
      return Future.error("null");
    }
    final filePath = join(directory.path, name);
    final bundleData = await rootBundle.load('assets/$name');
    List<int> bytes = bundleData.buffer.asUint8List(bundleData.offsetInBytes, bundleData.lengthInBytes);
    final file = await File(filePath).writeAsBytes(bytes);
    return file.path;
  }
}
