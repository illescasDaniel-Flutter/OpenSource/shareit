part of share_it;

@visibleForTesting
class ShareItConstants {
  static const methodChannel = 'shareit';
  static const iOSInvokeShareMethod = 'shareItiOS';
  static const androidInvokeShareMethod = 'shareItAndroid';
}
