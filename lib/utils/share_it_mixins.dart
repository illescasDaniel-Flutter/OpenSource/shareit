part of share_it;

mixin ShareItParametersMixin {
  _RawEnum<String> get type;
  String get content;
}
mixin ShareItMixin {
  List<ShareItParametersMixin> get parameters;
  Future<bool> present();
}
