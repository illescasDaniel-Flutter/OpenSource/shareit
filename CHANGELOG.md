## 0.7.0
Adapt to dart 3 and alllllll the new changes to dart, flutter and android... :)
## 0.6.0
Flutter v2 android embedding
## 0.5.1+3
- On iOS the share action sheet is now called asynchronously
## 0.4.1+2
- Tested behaviour of sharing files: 
    on Android < N you can share files saved on /sdcard
    on Android >= N you can share files from everywhere (AFAIK)
- Fixed sharing plain text on multiple items

## 0.4.0
Added support to same multiple items a the same time. 

## 0.3.1+3
minor improvements + fixed new "flutter (1.9) analyze" errors

## 0.3.0+1
iPad support + bug fixes

## 0.2.0+1
Bug fixes and added link option

## 0.1.0+2
Small package improvements

## 0.1.0+1
First release, works fine on both Android and iOS.
- Needs testing on iPad's and older Android versions.
